//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/15              //
//===============================//

#ifndef M_1_PARAMETERS_H
#define M_1_PARAMETERS_H

  const int MAX_LEN    = 100000; // max length of array to hold file string names
  const int OFF_SET    = 80;     // the offset
  const int ERROR_FLAG = -1;     // some constant integer as error return

#endif

//======//
// FINI //
//======//
