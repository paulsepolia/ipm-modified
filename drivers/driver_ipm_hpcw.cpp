//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/08/15              //
//===============================//

#include "m_1_parameters.h"
#include "m_2_read_dir.h"
#include "m_4_ipm_manip.h"
#include <sstream>
#include <string>
#include <stdlib.h>

using std::string;
using std::stringstream;

int main()
{
  //  1. Interface variables

  array<string,MAX_LEN> files_array; // array to hold the name of files
  int files_num;                     // number of files
  string user_output_old;            // name of user's output
  string ipm_output;                 // name of new ipm file
  string user_output_new;            // name of new user's output
  int debug_flag;                    // flag to enable debug output

  //  2. Local variables

  stringstream ss;
  string s;
  string s_tmp;
  string head_output_dir;
  int i;
  int out_ipm_manip;
  int out_read_dir;
  char* p_user;

  //  3-1. Local settings
 
  p_user = getenv("USER");
  ss << p_user;
  ss >> s_tmp;
  ss.str(std::string());
  ss.clear();

  //  3-2. Set local variables

  debug_flag = 0;

  //  4. Read current directory only

  out_read_dir = read_dir( files_array, \
                           files_num,   \
                           debug_flag );

  if (out_read_dir == ERROR_FLAG)
  { if( debug_flag == 1)
    { cout << " Driver 1 --> Error in 'out_read_dir' function." << endl; }}

  //  5. Call the manipulation function

  for (i = 0; i < files_num; i++)
  {
    // a. local inputs

    user_output_old = files_array[i];
    ss << i;
    ss >> s;
    ipm_output      = "ipm_only_" + s + ".txt";
    user_output_new = "output_new_" + s + ".txt";
    head_output_dir = "/home/pgg/ipm_reports/"+s_tmp+"_ipm_";
 
    // b. reset string stream - a must step

    ss.str(std::string());
    ss.clear();

    // c. output manipulation

    out_ipm_manip = ipm_manip( user_output_old, \
                               ipm_output,      \
                               user_output_new, \
                               head_output_dir, \
                               debug_flag );

    if (out_ipm_manip == ERROR_FLAG)
    { if( debug_flag == 1)
      { cout << " Driver 2 --> Error in 'out_ipm_manip' function." << endl; }}
  }

  //  6. Exit
 
  return 0;
}

//======//
// FINI //
//======//
